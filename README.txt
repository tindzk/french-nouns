This project aims to verify the hypothesis that the gender of French nouns
is supposed to be encoded in the suffix of the word. The phonology is taken
into account as well in order to find out whether the morphology or the
phonology is more significant for determining the gender.

TiMBL 6.4.2 was used to obtain the results which have been included in the
presentation.

To reproduce the results, you'll need the Larousse Chambers français-anglais
Babylon dictionary which I couldn't include due to licensing issues. The SHA1
hash of the .bgl file is: 4dc5b52c33d39e2c5cab060154a223be4a983fb0

Prior to running the scripts, you need to use dictconv to convert the .bgl
file into the .dic format.

The project is licensed under the terms of the EUPL v1.1.

--
Tim Nieradzik
timn@uni-bremen.de
