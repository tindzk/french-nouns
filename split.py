import sys

def writeLine(out, width, noun, gender):
	# Unicode sequence for nasal 'a': 91c9 83cc
	noun = noun.replace(b"\xc9\x91\xcc\x83".decode('utf8'), "A")

	# Unicode sequence for nasal 'e': 9bc9 83cc
	noun = noun.replace(b"\xc9\x9b\xcc\x83".decode('utf8'), "E")

	# Unicode sequence for nasal 'o': 94c9 83cc
	noun = noun.replace(b"\xc9\x94\xcc\x83".decode('utf8'), "O")

	# Unicode sequence for ʒ: 92ca
	noun = noun.replace(b"\xca\x92".decode('utf8'), "Z")

	for i in range(len(noun) - width, len(noun)):
		if i < 0:
			out.write("-")
		else:
			out.write(noun[i])

		out.write(" ")

	out.write(gender)
	out.write("\n")

for width in range(1, 11):
	for f in ('words', 'phon-words'):
		out = open(f + '-w' + str(width) + '.dat', 'w+')

		for line in open(f + '.txt'):
			parts = line.strip().split(' ')
			writeLine(out, width, parts[0], parts[1])