\documentclass{beamer}

\usepackage[utf8x]{inputenc}
\usepackage{tipa}
\usepackage{subfigure}

\usetheme{Amsterdam}

\title{Using memory-based learning to solve linguistic problems}
\author{Tim Nieradzik}
\institute{University of Bremen}
\date{15th February 2012}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}{Structure}
	\begin{enumerate}
		\item Linguistic problem
		\item Memory-based learning (MBL)
			\begin{enumerate}
				\item Process
				\item Terminology
			\end{enumerate}
		\item Application of MBL
		\item Results
		\item Analysis
		\item Conclusion
	\end{enumerate}
\end{frame}

\begin{frame}{Linguistic problem}
	\begin{itemize}
		\item Hypothesis: Gender of French nouns largely depends on the word suffix.
		\item Can the gender be predicted for an unknown noun?
		\item Is the gender also encoded in the phonetics?
	\end{itemize}
\end{frame}

\begin{frame}{Memory-based learning (MBL)}
	\begin{itemize}
		\item Natural language processing (NLP)
		\item Information retrieval (IR)
		\item Analogical reasoning
		\item Behaviour is stored ("memorised")
		\item Behaviour is extrapolated based on similarity
		\item Different approach: Rule-based processing
	\end{itemize}
\end{frame}

\begin{frame}{MBL: Process}
	\begin{enumerate}
		\item Acquire
		\item Extract
		\item Train
		\item Test
	\end{enumerate}
\end{frame}

\begin{frame}{MBL: Terminology}
	Training data comprises:
	\begin{itemize}
		\item Instances
		\item Features
		\item Classes
	\end{itemize}
\end{frame}

\begin{frame}[fragile]{Application of MBL}
	\textbf{Data source:} Larousse Chambers français-anglais

	\begin{itemize}
		\item Extracted 19398 nouns, their phonetics and genders.
		\item Create two separate files with nouns and phonetics.
	\end{itemize}

	\begin{figure}[h]
		\centering
		\begin{minipage}{.4\textwidth}
			stock m \\
			couvée f \\
			désamorçage m \\
			désunion f \\
			bouddhisme m
			\caption{Nouns}
		\end{minipage} % @todo don't insert empty line here
		\begin{minipage}{.4\textwidth}
			st\textipa{O}k m \\
			kuve f \\
			dezam\textipa{O}rsa\textipa{Z} m \\
			dezynj\textipa{\~O} f \\
			budism m
			\caption{Phonetic transcription (IPA)}
		\end{minipage}
	\end{figure}
\end{frame}

\begin{frame}[fragile]{Application of MBL (2)}
	\begin{itemize}
		\item Derive training data:
		\begin{itemize}
			\item Instances: Nouns
			\item Features: Last $n$ characters of word suffix or phonetics.
			\item Class: Male (m), female (f), both (mf)
		\end{itemize}
	\end{itemize}

	\begin{figure}[h]
		\centering
		\begin{minipage}{.4\textwidth}
			s t o c k m \\
			o u v é e f \\
			r ç a g e m \\
			u n i o n f \\
			h i s m e m
			\caption{Nouns}
		\end{minipage} % @todo don't insert empty line here
		\begin{minipage}{.4\textwidth}
			- s t \textipa{O} k m \\
			- k u v e f \\
			\textipa{O} r s a \textipa{Z} m \\
			z y n j \textipa{\~O} f \\
			u d i s m m
			\caption{Phonetic transcription (IPA)}
		\end{minipage}
	\end{figure}
\end{frame}

\begin{frame}[fragile]{Phonetics: Results}
	\begin{verbatim}
		Lines of data     : 19397
		DB Entropy        : 1.2886275
		Number of Classes : 3

		Feats   Vals    InfoGain        GainRatio
		    1     40    0.025686182     0.0057857861
		    2     42    0.045297450     0.010056933
		    3     40    0.071109185     0.015972679
		    4     41    0.20793257      0.047928668
		    5     37    0.31040875      0.072192428

		[...]

		overall accuracy:        0.807084  (15655/19397), of
		which 8768 exact matches
	\end{verbatim}
\end{frame}

\begin{frame}[fragile]{Suffixes: Results}
	\begin{verbatim}
		Lines of data     : 19398
		DB Entropy        : 1.2887599
		Number of Classes : 3

		Feats   Vals    InfoGain        GainRatio
		    1     64    0.082803258     0.019262099
		    2     65    0.14947136      0.035226914
		    3     58    0.19438457      0.047339363
		    4     58    0.23521005      0.059049829
		    5     50    0.28255530      0.10437327

		[...]

		overall accuracy:        0.861377  (16709/19398), of
		which 13351 exact matches
	\end{verbatim}
\end{frame}

\begin{frame}[fragile]{Suffixes: Confusion matrix}
	\begin{tabular}{|c|c|c|c|}
		\hline
		& m & f & mf \\
		\hline
		m & \textbf{8612} & 886 & 270 \\
		\hline
		f & 846 & \textbf{7350} & 95 \\
		\hline
		mf & 344 & 248 & \textbf{747} \\
		\hline
	\end{tabular}
\end{frame}

\begin{frame}[fragile]{Analysis}
	Most frequent suffixes and accuracies:
	\begin{figure}[h]
		\centering
		\begin{minipage}{.4\textwidth}
			\begin{verbatim}
				e (9812): 0.81
				n (2513): 0.97
				on (2068): 0.98
				t (1816): 0.97
				ion (1606): 0.99
				tion (1371): 1.00
				re (1339): 0.74
			\end{verbatim}
		\end{minipage} % @todo don't insert empty line here
		\begin{minipage}{.4\textwidth}
			\begin{verbatim}
				te (1332): 0.82
				r (1202): 0.91
				ie (1153): 0.96
				ation (1004): 1.00
				nt (967): 0.99
				é (875): 0.95
				ent (847): 1.00
			\end{verbatim}
		\end{minipage}
	\end{figure}

	Observations:
	\begin{itemize}
		\item $avg = 0.93$ for top 15 suffixes.
		\item Higher accuracies for longer suffixes, i.e. less general.
	\end{itemize}
\end{frame}

\begin{frame}[fragile]{Results for varying widths}
	\begin{tabular}{|c|c|c|c|c|c|}
		\hline
		& 1 & 2 & 3 & 4 & 5 \\
		\hline
		Suffix & 0.6848 & 0.7811 & 0.8526 & 0.8588 & \textbf{0.8613} \\
		\hline
		Phonetics & 0.69629 & 0.7934 & \textbf{0.8114} & 0.8089 & 0.8070 \\
		\hline
	\end{tabular}

	\begin{tabular}{|c|c|c|c|c|c|}
		\hline
		& 6 & 7 & 8 & 9 & 10 \\
		\hline
		Suffix & 0.8565 & 0.8540 & 0.8541 & 0.8544 & 0.8547 \\
		\hline
		Phonetics & 0.8044 & 0.80244 & 0.80033 & 0.7977 & 0.7964 \\
		\hline
	\end{tabular}
\end{frame}

\begin{frame}[fragile]{Conclusion}
	\begin{itemize}
		\item MBL well-suited for linguistic data analysis
		\item Verification of hypotheses
		\item In French gender is morphologically encoded in suffix
		\item Worse results when phonology is taken into account
		\item Future research:
			\begin{itemize}
				\item Induce rules from results
				\item Larger corpora
			\end{itemize}
	\end{itemize}
\end{frame}

\end{document}
