import sys
from collections import defaultdict

class Node(object):
	def __init__(self):
		self.pos  = 0
		self.neg  = 0
		self.next = defaultdict(int)

class Trie(object):
	def __init__(self):
		self.nodes = []
		self.nodes.append(Node())
		self.n = 1
		self.cntPositive = 0
		self.cntNegative = 0

	def addWord(self, word):
		curNode = 0

		for c in word:
			if self.nodes[curNode].next[c] != 0:
				curNode = self.nodes[curNode].next[c]
				continue

			self.nodes.append(Node())
			self.nodes[curNode].next[c] = self.n
			curNode = self.n
			self.n += 1

		return self.nodes[curNode]

	def addPositive(self, word):
		self.addWord(word).pos += 1
		self.cntPositive += 1

	def addNegative(self, word):
		self.addWord(word).neg += 1
		self.cntNegative += 1

	def countPositive(self):
		return self.cntPositive

	def countNegative(self):
		return self.cntNegative

	def heuristic(self, pos = 0, neg = 0):
		if pos + neg == 0:
			return 0

		return pos / (pos + neg)

	def getStats(self, node):
		pos, neg = (node.pos, node.neg)
		for key, value in node.next.items():
			cur = self.nodes[value]

			# @todo
			#pos, neg += self.getStats(cur)

			tmp = self.getStats(cur)
			pos += tmp[0]
			neg += tmp[1]

		return (pos, neg)

	def _getMostFrequent(self, entries, node, maxDepth, sequence = "", depth = 0):
		for key, value in node.next.items():
			cur = self.nodes[value]

			entries[sequence + key] += sum(self.getStats(cur))

			if depth + 1 < maxDepth:
				self._getMostFrequent(entries, cur, maxDepth, sequence + key, depth + 1)

	def getMostFrequent(self, depth, n = -1):
		entries = defaultdict(int)
		self._getMostFrequent(entries, self.nodes[0], depth)

		# Sort by frequency.
		sortedEntries = sorted(entries.items(), key=lambda t: t[1])

		if n != -1:
			# @todo validate `n'
			sortedEntries = sortedEntries[-n:]

		return list(reversed(sortedEntries))

	def getNode(self, word):
		curNode = 0
		for c in word:
			if self.nodes[curNode].next[c] == 0:
				raise AttributeError("No such word: %s" % word)

			curNode = self.nodes[curNode].next[c]

		if curNode == 0:
			raise AttributeError("No such word: %s" % word)

		return self.nodes[curNode]

	def __getitem__(self, word):
		"""Return the frequency of the given word."""
		curNode = self.getNode(word)

		# Now traverse all subnodes, and add up positive and negative numbers.
		totalPositive, totalNegative = self.getStats(curNode)

		return self.heuristic(totalPositive, totalNegative)

	def __len__(self):
		"""Return the number of nodes in the trie."""
		return self.n

positive = []
negative = []
suffixes = dict()

trie = Trie()

for line in open('words-w5.dat.LOO.O.gr.k1.out'):
	if line[0] == '#':
		continue

	parts = line.strip().split(' ')

	# Syntax: feat1 feat2 feat3 feat4 feat5 expected guessed
	# e m e n t m m
	# a n d o n m m

	if parts[5] == parts[6]:
		trie.addPositive(reversed(parts[0:5]))
	else:
		trie.addNegative(reversed(parts[0:5]))

print("positive: " + str(trie.countPositive()))
print("negative: " + str(trie.countNegative()))
print("ratio: %.2f" % trie.heuristic(
	trie.countPositive(),
	trie.countNegative()))

# Print number of nodes.
# print(len(trie))

# Accuracy for suffix -ment.
# print(trie[reversed("ment")])

print("Most frequent suffixes:")
perc = 0
for key, hits in trie.getMostFrequent(5, 15):
	suffix = key[::-1]
	ratio = trie[key]
	perc += ratio
	print("%s (%i): %.2f" % (suffix, hits, ratio))
print("Avg: %.2f" %  (perc / 15))
