# coding: utf8
import sys
import codecs

from collections import defaultdict
from BeautifulSoup import BeautifulStoneSoup

import re, htmlentitydefs
def unescape(text):
   """Removes HTML or XML character references
      and entities from a text string.
   @param text The HTML (or XML) source text.
   @return The plain text, as a Unicode string, if necessary.
   from Fredrik Lundh
   2008-01-03: input only unicode characters string.
   http://effbot.org/zone/re-sub.htm#unescape-html
   """
   def fixup(m):
      text = m.group(0)
      if text[:2] == "&#":
         # character reference
         try:
            if text[:3] == "&#x":
               return unichr(int(text[3:-1], 16))
            else:
               return unichr(int(text[2:-1]))
         except ValueError:
            print("Value Error")
            pass
      return text # leave as is
   return re.sub("&#?\w+;", fixup, text)

class Item:
	phonetics = ""
	feminine  = False
	masculine = False

results = defaultdict(Item)

def parseLine(s):
	parts = s.split("\t")

	keyword = unicode(parts[0], 'utf-8')
	posJunk = keyword.find("$")
	if posJunk != -1:
		keyword = keyword[0:posJunk]

	# Longest word form by including optional chracters.
	keyword = keyword.replace("(", "")
	keyword = keyword.replace(")", "")
	keyword = keyword.strip()

	# Skip all words containing spaces. Otherwise expressions some expressions
	# such as "on ne peut mieux" are recognised as nouns.
	if " " in keyword:
		return

	body    = unicode(parts[1], 'utf-8')
	entries = body.split("<HR>")

	for entry in entries:
		br = entry.find("<BR>")

		if br == -1:
			continue

		line = entry[0:br].strip()

		if len(line) == 0:
			continue

		if line[0] != '[':
			continue

		line = line.replace('<CHARSET c="T">', '&#x');
		line = line.replace('</CHARSET>', '');
		line = unescape(line)

		posNoun = line.find("nom ") # @todo also matches "pronom"

		if posNoun == -1:
			# Not a noun.
			continue

		noun = line[posNoun-1:].strip()

		if noun == "nom propre":
			continue

		# Overwrite gender from previous iteration(s) to prevent the following:
		# - g ê n e m f
		# - g ê n e mf m

		if noun.find(u"féminin") != -1:
			results[keyword].feminine = True

		if noun.find("masculin") != -1:
			results[keyword].masculine = True

		if not results[keyword].feminine and not results[keyword].masculine:
			continue

		bracket      = line.find("]")
		allPhonetics = re.sub(r'<FONT(.*)FONT>', '', line[1:bracket]).split(", ")

		# Only take into account the first option.
		phonetics = allPhonetics[0].strip()
		phonetics = phonetics.replace("(", "")
		phonetics = phonetics.replace(")", "")

		if len(phonetics) != 0:
			results[keyword].phonetics = phonetics

for line in open('larousse.dic'):
	parseLine(line)

for useKeyword in (True, False):
	if useKeyword:
		fn = 'words.txt'
	else:
		fn = 'phon-words.txt'

	out = codecs.open(fn, 'w+', encoding='utf-8')

	for key, value in results.items():
		if not value.feminine and not value.masculine:
			continue

		if useKeyword:
			if len(key) == 0:
				continue

			out.write(key)
		else:
			if len(value.phonetics) == 0:
				continue

			out.write(value.phonetics)

		out.write(" ")

		if value.masculine:
			out.write("m")

		if value.feminine:
			out.write("f")

		out.write("\n")

	out.close()
